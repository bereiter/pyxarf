#!/usr/bin/env python3

import logging
from pyxarf import Xarf
from xarfmail import XarfMail

# disable the next lines for less verbose output
logger = logging.getLogger('pyxarf')
logger.setLevel(logging.DEBUG)

report = Xarf(
    evidence='evidence data belongs here',
    schema_url='http://www.x-arf.org/schema/abuse_login-attack_0.1.2.json',
    schema_cache='/tmp/',
    reported_from='reporter@example.com',
    category='abuse',
    report_type='login-attack',
    report_id='1231231',
    date='Jan  1 2014 02:13:35 +0100',
    source='83.169.54.26',
    source_type='ip-address',
    attachment='text/plain',
    port=22,
    service='ssh',
)

human_readable = """
Dear Sir or Madam,

attached, you will find a report about an event
originating from a networks that you are listed as contact.

Best Regards,
  Mrs. Abuse
"""

mail_obj = XarfMail(
    report,
    mail_from = "cert@example.org",
    mail_to = "abuse@example.org",
    subject = "Abuse Report XYZ",
    greeting= human_readable
)

print(mail_obj)


